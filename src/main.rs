pub mod assets;
pub mod chunks;
pub mod terrain_material;

use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;
use bevy::window::PresentMode;
use bevy_atmosphere::prelude::{AtmosphereCamera, AtmospherePlugin};
use bevy_fly_camera::{FlyCamera, FlyCameraPlugin};
use crate::assets::AssetLoadingPlugin;
use crate::chunks::ChunkPlugin;
use crate::terrain_material::TerrainMaterialPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window{
                present_mode: PresentMode::AutoNoVsync,
                ..default()
            }),
            ..default()
        }))
        .add_plugins(FrameTimeDiagnosticsPlugin)
        .add_plugins(LogDiagnosticsPlugin::default())
        .add_plugins(FlyCameraPlugin)
        .add_plugins(AtmospherePlugin)
        .add_plugins(TerrainMaterialPlugin)
        .add_plugins(AssetLoadingPlugin)
        .add_plugins(ChunkPlugin)
        .add_systems(Startup, setup)
        .run();
}

#[derive(Component)]
pub struct MainCamera;

fn setup(mut commands: Commands) {
    commands
        .spawn(Camera3dBundle::default())
        .insert(FlyCamera{
            accel: 20.0,
            max_speed: 5.0,
            friction: 5.0,
            ..default()
        })
        .insert(AtmosphereCamera::default())
        .insert(MainCamera);
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            illuminance: 1000.0,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_translation(Vec3::ONE).looking_at(Vec3::ZERO, Vec3::Y),
        ..default()
    });
}

use bevy::asset::load_internal_asset;
use bevy::pbr::{MaterialPipeline, MaterialPipelineKey};
use bevy::prelude::*;
use bevy::reflect::TypePath;
use bevy::render::mesh::{MeshVertexAttribute, MeshVertexBufferLayout};
use bevy::render::render_resource::{AsBindGroup, RenderPipelineDescriptor, ShaderRef, ShaderType, SpecializedMeshPipelineError, VertexFormat};

use crate::chunks::chunk_data::TYPE_COUNT;

const TERRAIN_MATERIAL_HANDLE: Handle<Shader> = Handle::weak_from_u128(113427634837213758784550943013688339514);
const TERRAIN_MATERIAL_BINDINGS_HANDLE: Handle<Shader> = Handle::weak_from_u128(70346111776113120798345631908890889730);
// For some reason there is no uint16 ...
pub const ATTRIBUTE_TERRAIN_TYPE: MeshVertexAttribute =
    MeshVertexAttribute::new("TerrainType", 988540917, VertexFormat::Uint32);

pub struct TerrainMaterialPlugin;

impl Plugin for TerrainMaterialPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(MaterialPlugin::<TerrainMaterial>::default());
        load_internal_asset!(
            app,
            TERRAIN_MATERIAL_BINDINGS_HANDLE,
            concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/assets/shaders/terrain_material_bindings.wgsl"
            ),
            Shader::from_wgsl
        );
        load_internal_asset!(
            app,
            TERRAIN_MATERIAL_HANDLE,
            concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/assets/shaders/terrain_material.wgsl"
            ),
            Shader::from_wgsl
        );
    }
}

#[derive(Debug, Clone, ShaderType)]
pub struct BlockMaterial {
    pub color: Color,
    pub reflectance: f32,
    pub perceptual_roughness: f32,
    pub metallic: f32,
}

#[repr(C)]
#[derive(Asset, TypePath, AsBindGroup, Debug, Clone)]
pub struct TerrainMaterial {
    #[uniform(0)]
    pub materials: [BlockMaterial; TYPE_COUNT],
}

impl Material for TerrainMaterial {
    fn vertex_shader() -> ShaderRef {
        TERRAIN_MATERIAL_HANDLE.into()
    }
    fn fragment_shader() -> ShaderRef {
        TERRAIN_MATERIAL_HANDLE.into()
    }
    fn specialize(
        _pipeline: &MaterialPipeline<Self>,
        descriptor: &mut RenderPipelineDescriptor,
        layout: &MeshVertexBufferLayout,
        _key: MaterialPipelineKey<Self>,
    ) -> Result<(), SpecializedMeshPipelineError> {
        let vertex_layout = layout.get_layout(&[
            Mesh::ATTRIBUTE_POSITION.at_shader_location(0),
            Mesh::ATTRIBUTE_NORMAL.at_shader_location(1),
            Mesh::ATTRIBUTE_UV_0.at_shader_location(2),
            ATTRIBUTE_TERRAIN_TYPE.at_shader_location(7),
        ])?;
        descriptor.vertex.buffers = vec![vertex_layout];
        Ok(())
    }
}

use bevy::prelude::*;

pub const TYPE_COUNT: usize = 4;

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum BlockType {
    Air,
    Grass,
    Rock,
    Water,
}

impl BlockType {
    pub fn block_id(&self) -> u32 {
        match self {
            BlockType::Air => 0,
            BlockType::Grass => 1,
            BlockType::Rock => 2,
            BlockType::Water => 3,
        }
    }
    pub fn block_from_id(i: u32) -> BlockType {
        match i {
            0 => BlockType::Air,
            1 => BlockType::Grass,
            2 => BlockType::Rock,
            3 => BlockType::Water,
            _ => panic!("got block id that doesnt exist.")
        }
    }
}


#[derive(Component, Debug, Copy, Clone)]
pub struct ChunkBlock {
    pub block_type: BlockType,
}

pub struct ChunkData {
    // blocks: [ChunkBlock; 4096], //16x16x16
    pub(crate) blocks: Vec<ChunkBlock>, //16x16x128
}

impl ChunkData {
    pub const X: u32 = 18;
    pub const Y: u32 = 128;
    pub const Z: u32 = 18;
    pub const XZ: u32 = 324; // X * Z
    pub const PADDING: u32 = 1;

    #[inline]
    pub fn lin_to_dim(lin: u32) -> UVec3 {
        UVec3::new(lin % ChunkData::X, lin / ChunkData::XZ, (lin % ChunkData::XZ) / ChunkData::X)
    }
    #[inline]
    pub fn lin_to_dim_signed(lin: u32) -> IVec3 {
        Self::lin_to_dim(lin).as_ivec3()
    }
    #[inline]
    pub fn dim_to_lin(dim: UVec3) -> usize {
        (dim.x + ChunkData::X * dim.z + ChunkData::XZ * dim.y) as usize
    }

    pub fn is_air(&self, pos: IVec3) -> bool {
            self.block_type(pos) == BlockType::Air
    }
    pub fn block_type(&self, pos: IVec3) -> BlockType {
        if (0_i32..Self::X as i32).contains(&pos.x)
            && (0_i32..Self::Y as i32).contains(&pos.y)
            && (0_i32..Self::Z as i32).contains(&pos.z) {
            self.blocks[Self::dim_to_lin(pos.as_uvec3())].block_type
        } else {
            BlockType::Air
        }
    }
}


use bevy::prelude::*;

use crate::chunks::chunk_builder::ChunkBuilder;
use crate::chunks::chunk_data::BlockType;
use crate::chunks::chunk_manager::{ChunkManager, handle_tasks, spawn_chunk_tasks};

pub mod chunk_data;
pub mod chunk_mesher;
pub mod chunk_builder;
pub mod chunk_manager;

pub struct ChunkPlugin;

impl Plugin for ChunkPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::< ChunkManager>()
            .add_systems(Update, (spawn_chunk_tasks, handle_tasks).chain());
    }
}
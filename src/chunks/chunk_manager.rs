use std::sync::Arc;

use bevy::prelude::*;
use bevy::tasks::{AsyncComputeTaskPool, block_on, Task};
use bevy::tasks::futures_lite::future;
use bevy::utils::HashMap;

use crate::assets::TerrainMaterialHandle;
use crate::chunks::chunk_builder::ChunkBuilder;
use crate::chunks::chunk_data::ChunkData;
use crate::chunks::chunk_mesher::build_mesh;
use crate::MainCamera;

const RENDER_DISTANCE: i32 = 16;

#[derive(Component, Deref, DerefMut)]
pub struct ChunkPos(IVec2);

#[derive(Component)]
pub struct BuildChunkDataTask {
    task: Task<ChunkData>,
}

#[derive(Component)]
pub struct BuildChunkMeshTask {
    task: Task<Mesh>,
}

#[derive(Default, Resource)]
pub struct ChunkManager {
    //TODO: make multiple Hashmaps
    pub chunk_cache: HashMap<IVec2, (Option<Arc<ChunkData>>, Option<Entity>, Option<Handle<Mesh>>)>,
    pub chunk_builder: ChunkBuilder,
}

pub fn spawn_chunk_tasks(
    mut manager: ResMut<ChunkManager>,
    mut commands: Commands,
    camera_query: Query<&Transform, With<MainCamera>>,
    chunk_query: Query<(Entity, &ChunkPos)>,
    terrain_material_handle: Res<TerrainMaterialHandle>,
) {
    let thread_pool = AsyncComputeTaskPool::get();
    let position = camera_query.single().translation.xz();
    let chunk_position = position / Vec2::new((ChunkData::X - 2 * ChunkData::PADDING) as f32, (ChunkData::Z - 2 * ChunkData::PADDING) as f32);
    let current_chunk = chunk_position.floor().as_ivec2();
    for pos_x in -RENDER_DISTANCE..=RENDER_DISTANCE {
        for pos_z in -RENDER_DISTANCE..=RENDER_DISTANCE {
            let pos = current_chunk + IVec2::new(pos_x, pos_z);
            if let Some((_, entity, _)) = manager.chunk_cache.get(&pos) {
                if entity.is_some() {
                    continue;
                }
            }
            let mut entity_commands = commands.spawn(ChunkPos(pos));
            if let Some((Some(data), _, handle)) = manager.chunk_cache.get(&pos) {
                if let Some(handle) = handle {
                    entity_commands.insert(MaterialMeshBundle {
                        mesh: handle.clone(),
                        material: terrain_material_handle.clone_weak(),
                        transform: Transform::from_xyz(pos.x as f32 * 16.0, 0.0, pos.y as f32 * 16.0),
                        ..default()
                    });
                } else {
                    let data = data.clone();
                    entity_commands.insert(BuildChunkMeshTask {
                        task: thread_pool.spawn(async move {
                            build_mesh(data.as_ref())
                        }),
                    });
                }
            } else {
                let builder = manager.chunk_builder;
                let task = thread_pool.spawn(async move {
                    builder.build_chunk(pos)
                });
                entity_commands.insert(BuildChunkDataTask {
                    task,
                });
            }
            manager.chunk_cache.insert(pos, (None, Some(entity_commands.id()), None));
        }
    }
    for (e, chunk) in &chunk_query {
        let diff = current_chunk - **chunk;
        if diff.x.abs().max(diff.y.abs()) > RENDER_DISTANCE {
            if let Some(entity) = manager.chunk_cache.get_mut(&chunk.0).map(|(_, e, _)| e) {
                commands.entity(e).despawn();
                *entity = None;
            }
        }
    }
}

pub fn handle_tasks(
    mut commands: Commands,
    mut manager: ResMut<ChunkManager>,
    mut build_chunk_tasks: Query<(&mut BuildChunkDataTask, &ChunkPos, Entity)>,
    mut build_chunk_mesh_tasks: Query<(&mut BuildChunkMeshTask, &ChunkPos, Entity)>,
    mut meshes: ResMut<Assets<Mesh>>,
    terrain_material_handle: Res<TerrainMaterialHandle>,
) {
    for (mut task, pos, entity) in &mut build_chunk_tasks {
        if let Some(data) = block_on(future::poll_once(&mut task.task)) {
            // append the returned command queue to have it execute later
            manager.chunk_cache.get_mut(&**pos).unwrap().0 = Some(Arc::new(data));
            let data = manager.chunk_cache.get(&**pos).unwrap().0.clone().unwrap();
            let thread_pool = AsyncComputeTaskPool::get();
            commands.entity(entity).remove::<BuildChunkDataTask>().insert(BuildChunkMeshTask {
                task: thread_pool.spawn(async move {
                    build_mesh(&data)
                }),
            });
        }
    }
    for (mut task, pos, entity) in &mut build_chunk_mesh_tasks {
        if let Some(mesh) = block_on(future::poll_once(&mut task.task)) {
            let handle = meshes.add(mesh);
            let (_, ref mut e, ref mut h) = &mut manager.chunk_cache.get_mut(&**pos).unwrap();
            *h = Some(handle.clone());
            *e = Some(entity);

            commands.entity(entity).remove::<BuildChunkMeshTask>()
                .insert(MaterialMeshBundle {
                    mesh: handle,
                    material: terrain_material_handle.clone_weak(),
                    transform: Transform::from_xyz(pos.x as f32 * 16.0, 0.0, pos.y as f32 * 16.0),
                    ..default()
                });
        }
    }
}
use bevy::prelude::*;
use noise::{NoiseFn, OpenSimplex, Perlin};
use crate::chunks::chunk_data::{ChunkData, ChunkBlock, BlockType};

#[derive(Resource, Copy, Clone)]
pub struct ChunkBuilder {
    pub seed: u32,
    pub vertical_scale: f64,
    pub horizontal_scale: f64,
    pub noise_scale: f64,
    pub default_height: f64
}

impl Default for ChunkBuilder {
    fn default() -> Self {
       ChunkBuilder {
           seed: 0,
           vertical_scale: 0.05,
           noise_scale: 0.075,
           horizontal_scale: 0.02,
           default_height: 64.0,
       } 
    }
}

impl ChunkBuilder {
    pub fn build_chunk(&self, chunk_coords: IVec2) -> ChunkData {
        let offset = IVec3::new(chunk_coords.x * 16, 0, chunk_coords.y * 16);
        let noise = OpenSimplex::new(self.seed);
        let height_map = Perlin::new(self.seed);
        let mut blocks = Vec::with_capacity((ChunkData::X * ChunkData::Y * ChunkData::Z) as usize);
        for i in 0..blocks.capacity() {
            let pos = ChunkData::lin_to_dim(i as u32).as_ivec3();
            let world_pos = pos + offset;
            let world_pos_scaled = [world_pos.x as f64 * self.horizontal_scale, world_pos.y as f64 * self.vertical_scale, world_pos.z as f64 * self.horizontal_scale];
            let world_pos_noise_scaled = [world_pos.x as f64 * self.noise_scale, world_pos.y as f64 * self.noise_scale, world_pos.z as f64 * self.noise_scale];
            
            let terrain_height = height_map.get([world_pos_scaled[0], world_pos_scaled[2]]);
            let modifier = ((terrain_height / 2.0 + 0.5 ) * self.default_height  - world_pos.y as f64) / 64.0;
            let noise_value = noise.get(world_pos_noise_scaled);
            
            let mut block_type = if modifier + 0.5 * noise_value < 0.0 {
                BlockType::Air
            } else {
                BlockType::Grass
            };
            
            if pos.y < 10 {
                block_type = BlockType::Water;
            }
            
            if pos.y > 48  && block_type == BlockType::Grass{
                block_type = BlockType::Rock;
            }
            
            blocks.push(ChunkBlock{
                block_type,
            })
        }
        ChunkData {
            blocks
        }
    }
}

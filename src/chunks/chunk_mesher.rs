use bevy::prelude::*;

use crate::chunks::chunk_data::ChunkData;
use crate::terrain_material::ATTRIBUTE_TERRAIN_TYPE;

const DIRECTIONS: [IVec3; 6] = [
    IVec3::X,
    IVec3::NEG_X,
    IVec3::Y,
    IVec3::NEG_Y,
    IVec3::Z,
    IVec3::NEG_Z
];

pub struct ChunkMesh {
    pub mesh: Mesh,
}

impl ChunkMesh {
    pub fn push_face(&mut self, position: UVec3, normal: Vec3, terrain_type: u32) {
        let mut rect = Rectangle::new(1.0, 1.0).mesh();
        rect.transform_by(Transform::default().looking_at(-normal, Vec3::NEG_Y).with_translation(Vec3::new(position.x as f32, position.y as f32, position.z as f32) + normal * 0.5));
        rect.insert_attribute(ATTRIBUTE_TERRAIN_TYPE, vec![terrain_type; 4]);
        self.mesh.merge(rect);
    }
}

pub fn build_mesh(data: &ChunkData) -> Mesh {
    let mut mesh = ChunkMesh {
        mesh: Rectangle::new(0.0, 0.0).mesh(),
    };
    mesh.mesh.insert_attribute(ATTRIBUTE_TERRAIN_TYPE, vec![0_u32; 4]);

    for x in ChunkData::PADDING..(ChunkData::X - ChunkData::PADDING) {
        for y in 0..ChunkData::Y {
            for z in ChunkData::PADDING..(ChunkData::X - ChunkData::PADDING) {
                let pos = UVec3::new(x, y, z).as_ivec3();
                let terrain_type = data.block_type(pos).block_id();
                for dir in DIRECTIONS {
                    let new_pos = pos + dir;
                    if new_pos.y < 0 {
                        mesh.push_face(pos.as_uvec3(), dir.as_vec3(), terrain_type)
                    }
                    let is_air = data.is_air(pos);
                    let is_air_new = data.is_air(new_pos);
                    if !is_air && is_air_new {
                        mesh.push_face(pos.as_uvec3(), dir.as_vec3(), terrain_type)
                    }
                }
            }
        }
    }
    mesh.mesh
}
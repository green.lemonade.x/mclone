use bevy::prelude::*;

use crate::terrain_material::{BlockMaterial, TerrainMaterial};

pub struct AssetLoadingPlugin;

impl Plugin for AssetLoadingPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<TerrainMaterialHandle>();
    }
}

#[derive(Resource, Deref, DerefMut)]
pub struct TerrainMaterialHandle(pub Handle<TerrainMaterial>);

impl FromWorld for TerrainMaterialHandle {
    fn from_world(world: &mut World) -> Self {
        let mut materials = world.resource_mut::<Assets<TerrainMaterial>>();
        let air = BlockMaterial {
            color: Color::rgba(0.0, 0.0, 0.0, 0.0),
            reflectance: -1.0,
            perceptual_roughness: -1.0,
            metallic: -1.0,
        };
        let grass = BlockMaterial {
            color: Color::GREEN,
            reflectance: 1.0,
            perceptual_roughness: 1.0,
            metallic: 0.1,
        };
        let rock = BlockMaterial {
            color: Color::GRAY,
            reflectance: 1.0,
            perceptual_roughness: 1.0,
            metallic: 0.1,
        };
        let water = BlockMaterial {
            color: Color::BLUE,
            reflectance: 1.0,
            perceptual_roughness: 0.2,
            metallic: 1.0,
        };
        TerrainMaterialHandle(
            materials.add(
               TerrainMaterial {
                   materials: [air, grass, rock, water]
               } 
            )
        )
    }
}
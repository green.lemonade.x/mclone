#import bevy_pbr::mesh_functions as mesh_functions
#import bevy_pbr::mesh_bindings::mesh
#import bevy_pbr::mesh_view_bindings::view
#import bevy_core_pipeline::tonemapping::tone_mapping
#import bevy_pbr::pbr_types as pbr_types
#import bevy_pbr::pbr_functions as fns
#import mclone::terrain_material_bindings::{Vertex, MeshVertexOutput, terrain_material, BlockMaterial}
#ifdef PREPASS_PIPELINE
#import bevy_pbr::{
    prepass_io::{FragmentOutput},
    pbr_deferred_functions::deferred_output,
}
#else
#import bevy_pbr::{
    forward_io::{FragmentOutput},
    pbr_functions::{apply_pbr_lighting, main_pass_post_lighting_processing},
}
#import bevy_pbr::prepass_utils

#endif
@vertex
fn vertex(vertex: Vertex) -> MeshVertexOutput {
    var out: MeshVertexOutput;

    var model = mesh_functions::get_model_matrix(vertex.instance_index);

#ifdef VERTEX_NORMALS
    out.world_normal = mesh_functions::mesh_normal_local_to_world(
        vertex.normal,
        vertex.instance_index
    );
#endif

#ifdef VERTEX_POSITIONS
    out.world_position = mesh_functions::mesh_position_local_to_world(model, vec4<f32>(vertex.position, 1.0));
    out.position = mesh_functions::mesh_position_local_to_clip(model, vec4<f32>(vertex.position, 1.0));
#endif

#ifdef VERTEX_UVS
    out.uv = vertex.uv;
#endif

#ifdef VERTEX_TANGENTS
    out.world_tangent = mesh_functions::mesh_tangent_local_to_world(
        model,
        vertex.tangent,
        // Use vertex_no_morph.instance_index instead of vertex.instance_index to work around a wgpu dx12 bug.
        // See https://github.com/gfx-rs/naga/issues/2416
        vertex.instance_index
    );
#endif

#ifdef VERTEX_COLORS
    out.color = vertex.color;
#endif

#ifdef VERTEX_OUTPUT_INSTANCE_INDEX
    // Use vertex_no_morph.instance_index instead of vertex.instance_index to work around a wgpu dx12 bug.
    // See https://github.com/gfx-rs/naga/issues/2416
    out.instance_index = vertex.instance_index;
#endif
    out.terrain_type = vertex.terrain_type;

    return out;
}

fn get_material(terrain_type: u32) -> BlockMaterial {
    return terrain_material[terrain_type];
}

@fragment
// fn fragment(input: FragmentInput) -> @location(0) vec4<f32> {
// fn fragment(input: VertexOutput) -> @location(0) vec4<f32> {
fn fragment(input: MeshVertexOutput) -> FragmentOutput {
    var block_material = get_material(input.terrain_type);
    var pbr_input = pbr_types::pbr_input_new();

    pbr_input.flags = mesh[input.instance_index].flags;

    pbr_input.V = fns::calculate_view(input.world_position, false);
    pbr_input.frag_coord = input.position;
    pbr_input.world_position = input.world_position;

    pbr_input.world_normal = fns::prepare_world_normal(
        input.world_normal,
        false,
        false,
    );
#ifdef LOAD_PREPASS_NORMALS
    pbr_input.N = prepass_utils::prepass_normal(input.clip_position, 0u);
#else
    pbr_input.N = normalize(pbr_input.world_normal);
#endif

    pbr_input.material.base_color = block_material.color;

    pbr_input.material.reflectance = block_material.reflectance;
    pbr_input.material.perceptual_roughness = block_material.perceptual_roughness;
    pbr_input.material.metallic = block_material.metallic;


#ifdef PREPASS_PIPELINE
    // in deferred mode we can't modify anything after that, as lighting is run in a separate fullscreen shader.
    let out = deferred_output(in, pbr_input);
#else
    var out: FragmentOutput;
    // apply lighting
    out.color = apply_pbr_lighting(pbr_input);
    out.color = main_pass_post_lighting_processing(pbr_input, out.color);
#endif

    return out;
   // return vec4<f32>(input.blend_color, 1.0);
    // return vec4<f32>(input.blend_color * input.ambient, 1.0);
    // return vec4<f32>(1.0, 0.0,0.0,1.0);
}
